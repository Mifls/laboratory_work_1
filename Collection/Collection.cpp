#include "Collection.h"
#include <fstream>
#include <filesystem>

Collection::Collection() = default;

Collection::Collection(const Collection &object) {
    care_log = object.care_log;
    for (int i = 0; i < care_log.getSize(); ++i) {
        switch(care_log[i]->getType()) {
            case CUTTING_RECORD: {
                care_log[i] = new Cutting(*(Cutting *) (care_log[i]));
                break;
            }
            case WATERING_RECORD: {
                care_log[i] = new Watering(*(Watering *) (care_log[i]));
                break;
            }
        }
    }
}

Collection::~Collection() {
    clear();
//    printf("~\n");
}

//void Collection::addCuttingRecord(time_t date, const char* student, int course, int cut_branches) {
//    CareRecord *tmp;
//    tmp = new Cutting(date, student, course, cut_branches);
//    care_log.push_back(tmp);
//}
//
//void Collection::addWateringRecord(time_t date, const char* student, int course, float volume) {
//    CareRecord *tmp;
//    tmp = new Watering(date, student, course, volume);
//    care_log.push_back(tmp);
//}

void Collection::addRecord(const CareRecord *object) {
    CareRecord *tmp;
    switch(object->getType()) {
        case CUTTING_RECORD: {
            tmp = new Cutting(*(Cutting*)object);
            break;
        }
        case WATERING_RECORD: {
            tmp = new Watering(*(Watering*)object);
            break;
        }
    }
    care_log.push_back(tmp);
}

void Collection::deleteLast() {
    delete care_log.back();
    care_log.pop_back();
}

void Collection::clear() {
    for (int i = 0; i < care_log.getSize(); ++i) delete care_log[i];
    care_log.clear();
}

std::size_t Collection::getSize() const { return care_log.getSize(); };

bool Collection::isChronological() const {
    std::size_t size = care_log.getSize();
    if(size > 0) {
        time_t previous_date = care_log[0]->getDate();
        for (std::size_t i = 1; i < size; ++i) {
            time_t current_date = care_log[i]->getDate();
            if(current_date < previous_date) return false;
            previous_date = current_date;
        }
    }
    return true;
}

bool Collection::containsCuttingViolations() const {
    if(!isChronological()) return true;
    std::size_t size = care_log.getSize();
    std::size_t first_record = 0;
    for (; first_record < size; ++first_record) {
        if(care_log[first_record]->getType() == CUTTING_RECORD) break;
    }
    if(first_record < size) {
        auto t = care_log[first_record];
        int cutBranches = ((Cutting*)care_log[first_record])->getCutBranches();
        int first = 0;
        for (std::size_t i = first_record + 1; i < size; ++i) {
            if(care_log[i]->getType() == CUTTING_RECORD) {
                time_t current_date = care_log[i]->getDate();
                time_t first_date = care_log[first]->getDate();
                time_t last_date = care_log[i - 1]->getDate();

                float difference = std::difftime(current_date, last_date) /
                                   (60 * 60 * 24);
                if (difference > 7.0f) return true; //Была неделя без обрезки

                difference = std::difftime(current_date, first_date) /
                             (60 * 60 * 24);

                if (difference > 7.0f) {
                    if (cutBranches < 4) return true;
                }

                cutBranches += ((Cutting*)care_log[i])->getCutBranches();

                while (difference > 7.0f) {
                    cutBranches -= ((Cutting*)care_log[first])->getCutBranches();
                    first++;
                    first_date = care_log[first]->getDate();
                    difference = std::difftime(current_date, first_date) /
                                 (60 * 60 * 24);
                }

                if (cutBranches > 8) return true;
            }
        }
        if(cutBranches < 4)
            return true;
    }
    return false;
}

bool Collection::containsWateringViolations() const {
    if(!isChronological()) return true;
    std::size_t size = care_log.getSize();
    std::size_t first_record = 0;
    for (; first_record < size; ++first_record) {
        if(care_log[first_record]->getType() == WATERING_RECORD) break;
    }
    if(first_record < size) {
        time_t previous_date = care_log[first_record]->getDate();
        tm utc_previous = *gmtime(&previous_date);
        float currentVolume = ((Watering*)care_log[first_record])->getVolume();
        for (std::size_t i = first_record + 1; i < size; ++i) {
            if(care_log[i]->getType() == WATERING_RECORD) {
                time_t current_date = care_log[i]->getDate();
                tm utc_current = *gmtime(&current_date);
                if ((utc_current.tm_year == utc_previous.tm_year) &&
                    (utc_current.tm_yday == utc_previous.tm_yday)) {
                    currentVolume += ((Watering *) care_log[i])->getVolume();
                } else {
                    if ((currentVolume < 1.0f) && (currentVolume > 2.0f))
                        return true;
                    currentVolume = ((Watering *) care_log[i])->getVolume();
                    utc_previous = utc_current;
                }
            }
        }
        if((currentVolume < 1.0f) || (currentVolume > 2.0f))
            return true;
    }

    return false;
}

CareRecord& Collection::operator[](std::size_t idx) const {
    return *care_log[idx];
}

bool Collection::operator==(const Collection &object) const {
    if(care_log.getSize() != object.care_log.getSize()) return false;
    for (int i = 0; i < care_log.getSize(); ++i) {
        if(*care_log[i] != *object.care_log[i]) return false;
    }
    return true;
}

bool Collection::operator!=(const Collection &object) const {
    return !(*this == object);
}

void Collection::save(const char *path) const {
    std::ofstream output(path, std::ios::binary);
    std::size_t size = care_log.getSize();
    output.write((char *)&size, sizeof(size));
    for (std::size_t i = 0; i < size; ++i) {
        std::byte *buffer;
        std::size_t len = care_log[i]->toBytes(buffer);
        output.write((char *)&len, sizeof(len));
        output.write((char *)buffer, len);
        delete [] buffer;
    }
    output.close();
}

void Collection::load(const char *path) {
    if (std::filesystem::exists(path)) {
        std::ifstream input(path, std::ios::binary);
        std::size_t size;
        input.read((char *) &size, sizeof(size));
        for (std::size_t i = 0; i < size; ++i) {
            std::byte *buffer;
            std::size_t len;
            input.read((char *) &len, sizeof(len));
            buffer = new std::byte[len];
            input.read((char *) buffer, len);
            int type = *(int *)buffer;
            CareRecord *tmp;
            switch (type) {
                case CUTTING_RECORD: {
                    tmp = new Cutting();
                    ((Cutting*)tmp)->fromBytes(buffer + sizeof(int), len);
                    break;
                }

                case WATERING_RECORD: {
                    tmp = new Watering();
                    ((Watering*)tmp)->fromBytes(buffer + sizeof(int), len);
                    break;
                }
            }
            care_log.push_back(tmp);
            delete[] buffer;
        }
        input.close();
    }
}
