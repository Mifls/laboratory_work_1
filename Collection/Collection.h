#ifndef LABORATORY_WORK_1_COLLECTION_H
#define LABORATORY_WORK_1_COLLECTION_H

#include "Cutting/cutting.h"
#include "Watering/watering.h"
#include "Vector/vector.h"

class Collection {
private:
    Vector<CareRecord*> care_log;

public:
    Collection();
    Collection(const Collection &object);
    virtual ~Collection();
//    void addCuttingRecord(time_t date, const char *student, int course, int cut_branches);
//    void addWateringRecord(time_t date, const char *student, int course, float volume);
    void deleteLast();
    void clear();
    [[nodiscard]] std::size_t getSize() const;
    [[nodiscard]] bool isChronological() const;
    CareRecord& operator[](std::size_t idx) const;
    bool operator==(const Collection &object) const;
    bool operator!=(const Collection &object) const;
    void save(const char *path) const;
    void load(const char *path);

    [[nodiscard]] bool containsCuttingViolations() const;
    [[nodiscard]] bool containsWateringViolations() const;

    void addRecord(const CareRecord *object);
};


#endif //LABORATORY_WORK_1_COLLECTION_H
