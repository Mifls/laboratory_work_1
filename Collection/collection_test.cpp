#ifndef COLLECTION_TEST_CPP
#define COLLECTION_TEST_CPP

#include <cassert>
#include "Collection/Collection.h"

void printLog(const Collection &log);

void testCollection() {
    char a[] = "one", b[] = "two";
    Collection log;

    CareRecord *tmp = new Cutting(0, a, 1, 1);
    log.addRecord(tmp);
    assert(log.isChronological() == true);
    assert(log.containsCuttingViolations() == true); //Обрезано недостаточно ветвей
    assert(log[0].getName() == a);
    assert(log.getSize() == 1);
    delete tmp;

    tmp = new Watering(0, a, 1, 0.5f);
    log.addRecord(tmp);
    assert(log.isChronological() == true);
    assert(log.containsWateringViolations() == true); //Недостаточный полив
    assert(log[1].getName() == a);
    assert(log.getSize() == 2);
    delete tmp;

    tmp = new Cutting(1, b, 1, 5);
    log.addRecord(tmp);
    assert(log.isChronological() == true);
    assert(log.containsCuttingViolations() == false); //Нарушение устранено
    assert(log[0].getName() == a);
    assert(log[1].getName() == a);
    assert(log[2].getName() == b);
    assert(log.getSize() == 3);
    delete tmp;

    tmp = new Watering(1, b, 1, 1.0f);
    log.addRecord(tmp);
    assert(log.isChronological() == true);
    assert(log.containsWateringViolations() == false); //Нарушение устранено
    assert(log[0].getName() == a);
    assert(log[1].getName() == a);
    assert(log[2].getName() == b);
    assert(log[3].getName() == b);
    assert(log.getSize() == 4);
    delete tmp;

    tmp = new Cutting(1, b, 1, 3);
    log.addRecord(tmp);
    assert(log.isChronological() == true);
    assert(log.containsCuttingViolations() == true); //Обрезано слишком много ветвей
    assert(log.getSize() == 5);
    delete tmp;

    tmp = new Watering(1, b, 1, 1.0f);
    log.addRecord(tmp);
    assert(log.isChronological() == true);
    assert(log.containsWateringViolations() == true); //Избыточный полив
    assert(log.getSize() == 6);
    delete tmp;

    log.deleteLast();
    log.deleteLast();

    assert(log.isChronological() == true);
    assert(log.containsCuttingViolations() == false);
    assert(log.containsWateringViolations() == false);
    assert(log.getSize() == 4);

    tmp = new Watering(0, b, 1, 1.0f);
    log.addRecord(tmp);
    assert(log.isChronological() == false); //Нарушена хронология
    assert(log.containsWateringViolations() == true); //Нарушена хронология
    assert(log.getSize() == 5);
    delete tmp;

    Collection log_cpy(log);
    assert((log_cpy == log) == true);
    assert((log_cpy != log) == false);

    log_cpy[2].setCourse(2);
    assert((log_cpy == log) == false);
    assert((log_cpy != log) == true);

    log_cpy[2].setCourse(log[2].getCourse());
    log.clear();
    assert(log.getSize() == 0);
    assert((log_cpy == log) == false);
    assert((log_cpy != log) == true);

    log_cpy.save("./log");
    log.load("./log");
    assert((log_cpy == log) == true);
    assert((log_cpy != log) == false);

    printLog(log);
}

void printLog(const Collection &log) {
    auto size = log.getSize();
    for (int i = 0; i < size; ++i) {
        printf("### Record %d ###\n", i + 1);
        printf("%s\n", log[i].toString().data());
    }
}

#endif