#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QMessageBox>
#include <QFileDialog>

#include "Collection/Collection.h"

Collection collection;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_radioButton_clicked()
{
    if(ui->radioButton->isChecked()) {
        ui->label_4->setText("Объем воды (л):");
    }
}


void MainWindow::on_radioButton_2_clicked()
{
    if(ui->radioButton_2->isChecked()) {
        ui->label_4->setText("Обрезано ветвей:");
    }
}

void MainWindow::on_pushButton_clicked()
{
    time_t date = ui->dateEdit->dateTime().toTime_t();
    if(ui->lineEdit->text().isEmpty()) {
        QMessageBox::warning(this, "Ошибка","Введите Ф.И.О.!");
        return;
    }
    int course;
    bool ok;
    course = ui->lineEdit_2->text().toInt(&ok, 10);
    if(!ok || course < 1|| course > 5) {
        QMessageBox::warning(this, "Ошибка","Введите корректный курс [1..5]!");
        return;
    }
    if(ui->radioButton->isChecked()) {
        float volume = ui->lineEdit_3->text().toFloat(&ok);
        if(!ok || volume <= 0) {
            QMessageBox::warning(this, "Ошибка","Некорректный объем!");
            return;
        }
        std::string sname(ui->lineEdit->text().toStdString());
        char *name = new char[sname.length()];
        strncpy(name, sname.data(), sname.length());
        auto *tmp = new Watering(date, name, course, volume);
        collection.addRecord(tmp);
        QMessageBox::information(this, "Успех","Запись добавлена!");
    }
    else if(ui->radioButton_2->isChecked()) {
        int cut_branches = ui->lineEdit_3->text().toInt(&ok, 10);
        if(!ok || cut_branches <= 0) {
            QMessageBox::warning(this, "Ошибка","Некорректное число ветвей!");
            return;
        }
        std::string sname(ui->lineEdit->text().toStdString());
        char *name = new char[sname.length()];
        strncpy(name, sname.data(), sname.length());
        auto *tmp = new Cutting(date, name, course, cut_branches);
        collection.addRecord(tmp);
        QMessageBox::information(this, "Успех","Запись добавлена!");
    }

    updateCollection();

}

void MainWindow::updateCollection()
{
    if(collection.getSize() > 0)
        ui->label_5->setText(("Номер записи [1.." + std::to_string(collection.getSize()) + "]").data());
    else
        ui->label_5->setText("Номер записи (коллекция пуста):");


    ui->treeWidget->clear();
    for(int i = 0; i < collection.getSize(); i++) {
        QTreeWidgetItem *treeItem = new QTreeWidgetItem(ui->treeWidget);
        treeItem->setText(0, QString::number(i));

        QTreeWidgetItem *date = new QTreeWidgetItem();
        date->setText(0, "Дата");
        date->setText(1, QDateTime::fromTime_t(collection[i].getDate()).toString("dd.MM.yyyy"));
        treeItem->addChild(date);

        QTreeWidgetItem *name = new QTreeWidgetItem();
        name->setText(0, "ФИО");
        name->setText(1, QString::fromStdString(collection[i].getName()));
        treeItem->addChild(name);

        QTreeWidgetItem *course = new QTreeWidgetItem();
        course->setText(0, "Курс");
        course->setText(1, QString::number(collection[i].getCourse()));
        treeItem->addChild(course);

        QTreeWidgetItem *value = new QTreeWidgetItem();
        if(collection[i].getType() == WATERING_RECORD) {
            value->setText(0, "Объем воды");
            value->setText(1, QString::number(((Watering*)&collection[i])->getVolume()));
        }
        else if(collection[i].getType() == CUTTING_RECORD) {
            value->setText(0, "Обрезано ветвей");
            value->setText(1, QString::number(((Cutting*)&collection[i])->getCutBranches()));
        }
        treeItem->addChild(value);
    }

}


void MainWindow::on_pushButton_2_clicked()
{
    if(collection.getSize() == 0) {
        QMessageBox::warning(this, "Ошибка","Коллекция пуста!");
        return;
    }
    delete [] collection[collection.getSize() - 1].getName();
    collection.deleteLast();
    updateCollection();
}


void MainWindow::on_pushButton_3_clicked()
{
    bool ok;
    std::size_t n = ui->lineEdit_4->text().toInt(&ok, 10);
    if(!ok || n < 1 || n > collection.getSize()) {
        QMessageBox::warning(this, "Ошибка","Некорректное значетие или коллекция пуста!");
        return;
    }
    QMessageBox::information(this, "Запись №" + ui->lineEdit_4->text(), collection[n - 1].toString().data());
}


void MainWindow::on_pushButton_4_clicked()
{
    if(collection.isChronological())
        QMessageBox::information(this, "Проверка","Нарушения не найдены!");
    else
        QMessageBox::warning(this, "Проверка","Обнаружены нарушения!");
}


void MainWindow::on_pushButton_5_clicked()
{
    if(!collection.containsWateringViolations())
        QMessageBox::information(this, "Проверка","Нарушения не найдены!");
    else
        QMessageBox::warning(this, "Проверка","Обнаружены нарушения!");
}


void MainWindow::on_pushButton_6_clicked()
{
    if(!collection.containsCuttingViolations())
        QMessageBox::information(this, "Проверка","Нарушения не найдены!");
    else
        QMessageBox::warning(this, "Проверка","Обнаружены нарушения!");
}


void MainWindow::on_pushButton_8_clicked()
{
    auto fileName = QFileDialog::getOpenFileName(this,
             tr("Open File"), "~/", tr("All Files (*)"));
    collection.load(fileName.toStdString().data());
    updateCollection();
}


void MainWindow::on_pushButton_7_clicked()
{
    auto fileName = QFileDialog::getSaveFileName(this,
             tr("Save File"), "~/", tr("All Files (*)"));
    collection.save(fileName.toStdString().data());
    updateCollection();
}

