#include "carelog.h"
#include <fstream>
#include <filesystem>

template <class T>
CareLog<T>::CareLog() = default;

template <class T>
CareLog<T>::CareLog(const CareLog &object) {
    care_log = object.care_log;
}

template <class T>
CareLog<T>::~CareLog() {
    care_log.clear();
//    printf("~\n");
}

template <class T>
template <typename ... Args> void CareLog<T>::addRecord(const Args& ... args) {
    care_log.emplace_back(args ...);
}

template <class T>
void CareLog<T>::deleteLast() { care_log.pop_back(); }

template <class T>
void CareLog<T>::clear() { care_log.clear(); }

template <class T>
std::size_t CareLog<T>::getSize() const { return care_log.getSize(); };

template <class T>
bool CareLog<T>::isChronological() const {
    std::size_t size = care_log.getSize();
    if(size > 0) {
        time_t previous_date = care_log[0].getDate();
        for (std::size_t i = 1; i < size; ++i) {
            time_t current_date = care_log[i].getDate();
            if(current_date < previous_date) return false;
            previous_date = current_date;
        }
    }
    return true;
}

template <class T>
T& CareLog<T>::operator[](std::size_t idx) const {
    return care_log[idx];
}

template <class T>
bool CareLog<T>::operator==(const CareLog &object) const {
    return (care_log == object.care_log);
}

template <class T>
bool CareLog<T>::operator!=(const CareLog &object) const {
    return (care_log != object.care_log);
}

template <class T>
void CareLog<T>::save(const char *path) const {
    std::ofstream output(path, std::ios::binary);
    std::size_t size = care_log.getSize();
    output.write((char*)&size, sizeof(size));
    for (std::size_t i = 0; i < size; ++i) {
        std::byte *buffer;
        std::size_t len = care_log[i].toBytes(buffer);
        output.write((char*)&len, sizeof(len));
        output.write((char*)buffer, len);
        delete [] buffer;
    }
    output.close();
}

template <class T>
void CareLog<T>::load(const char *path) {
    if (std::filesystem::exists(path)) {
        std::ifstream input(path, std::ios::binary);
        std::size_t size;
        input.read((char *) &size, sizeof(size));
        for (std::size_t i = 0; i < size; ++i) {
            std::byte *buffer;
            std::size_t len;
            input.read((char *) &len, sizeof(len));
            buffer = new std::byte[len];
            input.read((char *) buffer, len);
            care_log.emplace_back();
            care_log.back().fromBytes(buffer, len);
            delete[] buffer;
        }
        input.close();
    }
}