#ifndef LABORATORY_WORK_1_CARELOG_H
#define LABORATORY_WORK_1_CARELOG_H

#include "Vector/vector.h"

template <class T>
class CareLog {
protected:
    Vector<T> care_log;

public:
    CareLog();
    CareLog(const CareLog &object);
    virtual ~CareLog();
    template <typename ... Args> void addRecord(const Args& ... args);
    void deleteLast();
    void clear();
    [[nodiscard]] std::size_t getSize() const;
    [[nodiscard]] bool isChronological() const;
    T& operator[](std::size_t idx) const;
    bool operator==(const CareLog &object) const;
    bool operator!=(const CareLog &object) const;
    void save(const char *path) const;
    void load(const char *path);
};

#include "carelog.cpp"

#endif //LABORATORY_WORK_1_CARELOG_H
