#ifndef CARE_TEST_CPP
#define CARE_TEST_CPP

#include <cassert>
#include "carerecord.h"
#include "carelog.h"
#include "Cutting/cutting.h"

void testCareRecord() {
    auto care = new CareRecord;
    assert(care);
    assert(care->getCourse() == 1);
    assert(care->getDate() > 0);
    assert(care->getName() == nullptr);

    delete care;

    char name[] = "Vasya";
    care = new CareRecord(1000, name, 2);
    assert(care);
    assert(care->getCourse() == 2);
    assert(care->getDate() == 1000);
    assert(care->getName() == name);

    CareRecord copy_care = *care;

    assert(copy_care.getCourse() == care->getCourse());
    assert(copy_care.getDate() == care->getDate());
    assert(copy_care.getName() == care->getName());

    delete care;

    copy_care.setCourse(3);
    assert(copy_care.getCourse() == 3);
    copy_care.setDate(2000);
    assert(copy_care.getDate() == 2000);
}

#endif