#ifndef LABORATORY_WORK_1_CARERECORD_H
#define LABORATORY_WORK_1_CARERECORD_H

#include <ctime>
#include <stdexcept>
#include <cstring>
#include "RecordTypes.h"

class CareRecord {
protected:
    time_t date;
    const char* student;
    int course;

public:
    CareRecord();
    CareRecord(time_t date, const char* student, int course);
    CareRecord(const CareRecord &object);
    virtual ~CareRecord();
    [[nodiscard]] time_t getDate() const;
    void setDate(time_t date);
    [[nodiscard]] const char* getName() const;
    void setName(const char* student);
    [[nodiscard]] int getCourse() const;
    void setCourse(int course);
    [[nodiscard]] virtual RecordTypes getType() const;
    [[nodiscard]] virtual std::string toString() const;
    virtual std::size_t toBytes(std::byte *&ptr) const;
    bool operator==(const CareRecord &object) const;
    bool operator!=(const CareRecord &object) const;
};


#endif //LABORATORY_WORK_1_CARERECORD_H
