#include "carerecord.h"

CareRecord::CareRecord() {
    this->date = std::time(0);
    this->student = nullptr;
    this->course = 1;
}

CareRecord::CareRecord(time_t date, const char* student, int course) {
    this->date = date;
    this->student = student;
    if((course < 1) || (course > 5)) {
        throw std::invalid_argument( "Course: a value between 1 and 5 is expected" );
    }
    else {
        this->course = course;
    }
}

CareRecord::CareRecord(const CareRecord &object) {
    this->date = object.date;
    this->student = object.student;
    this->course = object.course;
}

CareRecord::~CareRecord() {
    this->student = nullptr;
}

time_t CareRecord::getDate() const { return date; }

void CareRecord::setDate(time_t date) { this->date = date; }

const char* CareRecord::getName() const { return student; }

void CareRecord::setName(const char* student) { this->student = student; }

int CareRecord::getCourse() const { return course; }

void CareRecord::setCourse(int course) {
    if((course < 1) || (course > 5)) {
        throw std::invalid_argument( "Course: a value between 1 and 5 is expected" );
    }
    else {
        this->course = course;
    }
}

bool CareRecord::operator==(const CareRecord &object) const {
    if(this->date != object.date) return false;
    if(this->course != object.course) return false;
    if(strcmp(this->student, object.student) != 0) return false;
    return true;
}

bool CareRecord::operator!=(const CareRecord &object) const {
    return !(*this == object);
}

std::size_t CareRecord::toBytes(std::byte *&ptr) const {
    return 0;
}

RecordTypes CareRecord::getType() const {
    return BASE_RECORD;
}

std::string CareRecord::toString() const {
    return "";
}
