# Лабораторная работа 1

## Вариант 12 (https://oop-miem.github.io/#l1v12)

### К проверке
    
    Исправлены замечания к ч.1:
        -Разбиение h/cpp
        -Устранение излишнего копирования
        -Доработка тестирования

    Часть 2:
        -файл "main.cpp"
        -папка "Vector/"
        -папка "Cutting/"
        -папка "Watering/"
        *см. структуру


### Структура

"main.cpp"
    -Зпуск тестов
    -Реализация функций печати коллекций
    -Печать коллекций

"Vector/"
    "vector.h/.cpp" - реализация вектора (согласно ограничению на использование готовых реализаций)
    "vector_test.cpp" - тестирование

"Cutting/"
    "cutting.h/.cpp" - класс записи в журнале обрезки
    "cuttinglog.h/.cpp" - журнал обрезки (на основе Vector)
    "cutting_test.cpp" - тестирование

"Watering/"
    "watering.h/.cpp" - класс записи в журнале полива
    "wateringlog.h/.cpp" - журнал полива (на основе Vector)
    "watering_test.cpp" - тестирование
