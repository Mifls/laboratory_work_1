#include <cstring>
#include "vector.h"

//private:

//Изменяет размер вектора, при этом вектор перемещается
template <class T>
void Vector<T>::resize(std::size_t new_size) {
    reserved = new_size;
    auto tmp = (T*)malloc(sizeof(T) * reserved);
    //Если вектор "обрезается", не нужно копировать лишнее
    size = std::min(reserved, size);
    std::memcpy(tmp, array, sizeof(T) * size);
    free(array); //Нам не нужно вызывать деструктор при копировании
    array = tmp;
}

//public:

//По-умолчанию вектор пуст
template <class T>
Vector<T>::Vector() {
    array = nullptr;
    size = 0;
    reserved = 0;
}

template <class T>
Vector<T>::Vector(const Vector<T> &object) {
    array = nullptr; //Чтобы не запросить освобождение невыделенной памяти
    size = 0;
    resize(object.size); //Дополнительное место не резервируется
    size = object.size;
    std::memcpy(array, object.array, sizeof(T) * size);
}

template <class T>
Vector<T>::~Vector() {
    free(array);
}

template <class T>
void Vector<T>::push_back(const T &x) {
    //Если нет зарезервированного места, нужно его выделить
    //
    //Дополнительное место резервируется для
    //уменьшения частоты выделения памяти (O(malloc) ~ 100)
    //(на подобии std::vector)
    if(reserved <= size) resize(size + sqrt(size) + 1);
    array[size] = x;
    size++;
}

template <class T>
template <typename ... Args> void Vector<T>::emplace_back(const Args& ... args) {
    if(reserved >= size) resize(size + sqrt(size) + 1);
    array[size] = T(args...);
    size++;
}

template <class T>
void Vector<T>::pop_back() {
    if(size > 0) {
        size--;
        //Если резерв слишком большой, нужно освободить память
        if(reserved > size + sqrt(size) + 1)
            resize(size);
    }
}

template <class T>
T& Vector<T>::back() const {
    //Проверка нарушения памяти
    if(size == 0)
        throw std::invalid_argument( "Out of range" );
    return array[size - 1];
}

template <class T>
void Vector<T>::clear() {
    if(array != nullptr) free(array);
    array = nullptr;
    size = 0;
    reserved = 0;
}

template <class T>
std::size_t Vector<T>::getSize() const { return size; }

template <class T>
T& Vector<T>::operator[](std::size_t idx) const {
    //Проверка нарушения памяти
    if(idx>size)
        throw std::invalid_argument( "Out of range" );
    return array[idx];
}

template <class T>
Vector<T>& Vector<T>::operator=(const Vector<T> &object) noexcept {
    if(&object != this) {
        clear();
        resize(object.size); //Дополнительное место не резервируется
        size = object.size;
        std::memcpy(array, object.array, sizeof(T) * size);
    }
}

template <class T>
bool Vector<T>::operator==(const Vector<T> &object) const {
    //Оптимизация
    if(size != object.size) return false;
    //Поэлементное сравнение позволяет использовать перегруженный оператор ==
    for (std::size_t i = 0; i < size; ++i)
        if(array[i] != object.array[i]) return false;
    return true;
}

template <class T>
bool Vector<T>::operator!=(const Vector<T> &object) const { return !(*this == object); }