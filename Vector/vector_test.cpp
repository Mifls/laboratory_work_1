#ifndef VECTOR_TEST_H
#define VECTOR_TEST_H

#include <cassert>
#include "vector.h"

void testVector() {
    Vector<char*> vec;
    assert(vec.getSize() == 0);

    char a[] = "one", b[] = "two";

    //Тестирование добавления элементов
    vec.push_back(a);
    assert(vec.getSize() == 1);
    assert(vec[0] == a);

    vec.push_back(b);
    assert(vec.getSize() == 2);
    assert(vec[0] == a);
    assert(vec[1] == b);

    //Тестирование удаления элементов
    vec.pop_back();
    assert(vec.getSize() == 1);
    assert(vec[0] == a);

    vec.push_back(b);

    assert(vec.getSize() == 2);
    assert(vec[0] == a);
    assert(vec[1] == b);

    //Проверка конструктора копирования и операторов сравнения
    Vector<char*> vec_cpy(vec);
    assert(vec == vec_cpy);
    assert(!(vec != vec_cpy));
    vec_cpy[1] = a;
    assert(!(vec == vec_cpy));
    assert(vec != vec_cpy);

    //Тестирование очистки
    vec.clear();
    vec_cpy.clear();
    assert(vec.getSize() == 0);

    vec.push_back(b);
    assert(vec.getSize() == 1);
    assert(vec[0] == b);
}

#endif