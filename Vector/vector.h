#ifndef LABORATORY_WORK_1_VECTOR_H
#define LABORATORY_WORK_1_VECTOR_H

#include <cmath>
#include <algorithm>

template <class T>
class Vector {
private:
    T *array;
    std::size_t size;
    std::size_t reserved;

    void resize(std::size_t new_size);
public:
    Vector();
    Vector(const Vector<T> &object);
    ~Vector();

    void push_back(const T &x);
    template <typename ... Args> void emplace_back(const Args& ... args);
    void pop_back();
    T& back() const;
    void clear();
    [[nodiscard]] std::size_t getSize() const;
    T& operator[](std::size_t idx) const;
    Vector<T>& operator=(const Vector<T> &other) noexcept;
    bool operator==(const Vector<T> &object) const;
    bool operator!=(const Vector<T> &object) const;
};
#include "vector.cpp"

#endif //LABORATORY_WORK_1_VECTOR_H
