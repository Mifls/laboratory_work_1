#ifndef LABORATORY_WORK_1_WATERINGLOG_H
#define LABORATORY_WORK_1_WATERINGLOG_H

#include "watering.h"
#include "Care/carelog.h"

class WateringLog : public CareLog<Watering>{

public:
    WateringLog();
    WateringLog(const WateringLog &object);
    virtual ~WateringLog();
    [[nodiscard]] bool containsViolations() const;

};


#endif //LABORATORY_WORK_1_WATERINGLOG_H
