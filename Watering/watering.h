#ifndef UNTITLED1_WATERING_H
#define UNTITLED1_WATERING_H

#include "Care/carerecord.h"

class Watering : public CareRecord {
private:
    float volume;

public:
    Watering();
    Watering(time_t date, const char* student, int course, float volume);
    Watering(const Watering &object);
    ~Watering() override;
    [[nodiscard]] float getVolume() const;
    void setVolume(float volume);
    [[nodiscard]] RecordTypes getType() const override;
    [[nodiscard]] std::string toString() const override;
    [[nodiscard]] std::size_t toBytes(std::byte *&ptr) const override;
    void fromBytes(std::byte *ptr, std::size_t len);
    bool operator==(const Watering &object) const;
    bool operator!=(const Watering &object) const;
};


#endif //UNTITLED1_WATERING_H
