#include <fstream>
#include <filesystem>
#include "wateringlog.h"

WateringLog::WateringLog() = default;

WateringLog::WateringLog(const WateringLog &object) : CareLog<Watering>(object) {}

WateringLog::~WateringLog() = default;

bool WateringLog::containsViolations() const {
    if(!isChronological()) return true;
    std::size_t size = care_log.getSize();
    if(size > 0) {
        time_t previous_date = care_log[0].getDate();
        tm utc_previous = *gmtime(&previous_date);
        float currentVolume = care_log[0].getVolume();
        for (std::size_t i = 1; i < size; ++i) {
            time_t current_date = care_log[i].getDate();
            tm utc_current = *gmtime(&current_date);
            if((utc_current.tm_year == utc_previous.tm_year) &&
               (utc_current.tm_yday == utc_previous.tm_yday)) {
                currentVolume += care_log[i].getVolume();
            }
            else {
                if((currentVolume < 1.0f) && (currentVolume > 2.0f))
                    return true;
                currentVolume = care_log[i].getVolume();
                utc_previous = utc_current;
            }
        }
        if((currentVolume < 1.0f) || (currentVolume > 2.0f))
            return true;
    }

    return false;
}
