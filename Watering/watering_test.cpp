#ifndef WATERING_TEST_CPP
#define WATERING_TEST_CPP

#include <cassert>
#include "watering.h"
#include "wateringlog.h"

void testWateringRecord() {
    auto wtr = new Watering;
    assert(wtr);
    assert(wtr->getCourse() == 1);
    assert(wtr->getDate() > 0);
    assert(wtr->getName() == nullptr);
    assert(wtr->getVolume() == 0.0f);

    delete wtr;

    char name[] = "Vasya";
    wtr = new Watering(1000, name, 2, 2.0f);
    assert(wtr);
    assert(wtr->getCourse() == 2);
    assert(wtr->getDate() == 1000);
    assert(wtr->getName() == name);
    assert(wtr->getVolume() == 2.0f);

    Watering copy_wtr = *wtr;

    assert(copy_wtr.getCourse() == wtr->getCourse());
    assert(copy_wtr.getDate() == wtr->getDate() );
    assert(copy_wtr.getName() == wtr->getName());
    assert(copy_wtr.getVolume() == wtr->getVolume());

    delete wtr;

    copy_wtr.setCourse(3);
    assert(copy_wtr.getCourse() == 3);
    copy_wtr.setDate(2000);
    assert(copy_wtr.getDate() == 2000);
    copy_wtr.setVolume(3.0f);
    assert(copy_wtr.getVolume() == 3.0f);
}

#endif