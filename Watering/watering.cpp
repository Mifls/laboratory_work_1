#include <cstring>
#include "watering.h"

Watering::Watering() : CareRecord(){
    this->volume = 0.0f;
}

Watering::Watering(time_t date, const char* student, int course, float volume)
        : CareRecord(date, student, course) {
    if(volume < 0.0f) {
        throw std::invalid_argument( "Volume: negative value received" );
    }
    else {
        this->volume = volume;
    }

}

Watering::Watering(const Watering &object) : CareRecord(object){
    this->volume = object.volume;
}

Watering::~Watering() {
    this->student = nullptr;
}

float Watering::getVolume() const { return volume; }
void Watering::setVolume(float volume) {
    if(volume < 0.0f) {
        throw std::invalid_argument( "Volume: negative value received" );
    }
    else {
        this->volume = volume;
    }
}

std::string Watering::toString() const {
    std::string str;
    struct tm *timeinfo;
    timeinfo = localtime ( &date );
    str = "Date: " + std::string(std::asctime(timeinfo));
    str += "Student: " + std::string(student) + "\n";
    str += "Course: " + std::to_string(course) + "\n";
    str += "Volume: " + std::to_string(volume) + "\n";
    return str;
}

std::size_t Watering::toBytes(std::byte *&ptr) const {
    std::size_t len = sizeof(int) + sizeof(Watering) -
                      - sizeof(char*) + (strlen(student) + 1);
    ptr = new std::byte[len];
    auto tmp = ptr;
    *(int*)tmp = WATERING_RECORD;
    tmp += sizeof(int);
    memcpy(tmp, &date, sizeof(date));
    tmp += sizeof(date);
    memcpy(tmp, &course, sizeof(course));
    tmp += sizeof(course);
    memcpy(tmp, &volume, sizeof(volume));
    tmp += sizeof(volume);
    //ФИО записывается в конец, чтобы не хранить его длину
    memcpy(tmp, student, (strlen(student) + 1));
    return len;
}

void Watering::fromBytes(std::byte *ptr, std::size_t len) {
    std::size_t name_len = len - sizeof(date)
                           - sizeof(course) - sizeof(volume);
    memcpy(&date, ptr, sizeof(date));
    ptr += sizeof(date);
    memcpy(&course, ptr, sizeof(course));
    ptr += sizeof(course);
    memcpy(&volume, ptr, sizeof(volume));
    ptr += sizeof(volume);
    student = new char[name_len];
    memcpy((char *) student, ptr, name_len);
}

bool Watering::operator==(const Watering &object) const {
    if(this->CareRecord::operator!=(object)) return false;
    if(this->volume != object.volume) return false;
    return true;
}

bool Watering::operator!=(const Watering &object) const {
    return !(*this == object);
}

RecordTypes Watering::getType() const {
    return WATERING_RECORD;
}
