#ifndef CUTTING_TEST_CPP
#define CUTTING_TEST_CPP

#include <cassert>
#include "cutting.h"
#include "cuttinglog.h"

void testCuttingRecord() {
    auto ctg = new Cutting;
    assert(ctg);
    assert(ctg->getCourse() == 1);
    assert(ctg->getDate() > 0);
    assert(ctg->getName() == nullptr);
    assert(ctg->getCutBranches() == 0);

    delete ctg;

    char name[] = "Vasya";
    ctg = new Cutting(1000, name, 2, 2);
    assert(ctg);
    assert(ctg->getCourse() == 2);
    assert(ctg->getDate() == 1000);
    assert(ctg->getName() == name);
    assert(ctg->getCutBranches() == 2);

    Cutting copy_ctg = *ctg;

    assert(copy_ctg.getCourse() == ctg->getCourse());
    assert(copy_ctg.getDate() == ctg->getDate());
    assert(copy_ctg.getName() == ctg->getName());
    assert(copy_ctg.getCutBranches() == ctg->getCutBranches());

    delete ctg;

    copy_ctg.setCourse(3);
    assert(copy_ctg.getCourse() == 3);
    copy_ctg.setDate(2000);
    assert(copy_ctg.getDate() == 2000);
    copy_ctg.setCutBranches(3);
    assert(copy_ctg.getCutBranches() == 3);
}

#endif