#ifndef LABORATORY_WORK_1_CUTTINGLOG_H
#define LABORATORY_WORK_1_CUTTINGLOG_H

#include "cutting.h"
#include "Care/carelog.h"

class CuttingLog : public CareLog<Cutting>{

public:
    CuttingLog();
    CuttingLog(const CuttingLog &object);
    virtual ~CuttingLog();
    [[nodiscard]] bool containsViolations() const;
};


#endif //LABORATORY_WORK_1_CUTTINGLOG_H
