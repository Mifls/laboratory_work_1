#include <fstream>
#include <filesystem>
#include "cuttinglog.h"

CuttingLog::CuttingLog() = default;

CuttingLog::CuttingLog(const CuttingLog &object) : CareLog<Cutting>(object) {}

CuttingLog::~CuttingLog() = default;

bool CuttingLog::containsViolations() const {
    if(!isChronological()) return true;
    std::size_t size = care_log.getSize();
    if(size > 0) {
        auto t = care_log[0];
        int cutBranches = care_log[0].getCutBranches();
        int first = 0;
        for (int i = 1; i < size; ++i) {
            time_t current_date = care_log[i].getDate();
            time_t first_date = care_log[first].getDate();
            time_t last_date = care_log[i - 1].getDate();

            float difference = std::difftime(current_date, last_date) /
                               (60 * 60 * 24);
            if(difference > 7.0f) return true; //Была неделя без обрезки

            difference = std::difftime(current_date, first_date) /
                         (60 * 60 * 24);

            if(difference > 7.0f) {
                if(cutBranches < 4) return true;
            }

            cutBranches += care_log[i].getCutBranches();

            while(difference > 7.0f) {
                cutBranches -= care_log[first].getCutBranches();
                first++;
                first_date = care_log[first].getDate();
                difference = std::difftime(current_date, first_date) /
                             (60 * 60 * 24);
            }

            if(cutBranches > 8) return true;
        }
        if(cutBranches < 4)
            return true;
    }
    return false;
}