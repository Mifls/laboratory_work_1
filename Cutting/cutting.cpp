#include <cstring>
#include "cutting.h"

Cutting::Cutting() : CareRecord(){
    this->cut_branches = 0;
}

Cutting::Cutting(time_t date, const char* student, int course, int cut_branches)
        : CareRecord(date, student, course) {
    if(cut_branches < 0) {
        throw std::invalid_argument( "Сut_branches: negative value received" );
    }
    else {
        this->cut_branches = cut_branches;
    }
}

Cutting::Cutting(const Cutting &object) : CareRecord(object){
    this->cut_branches = object.cut_branches;
}

Cutting::~Cutting() {
    this->student = nullptr;
}

int Cutting::getCutBranches() const { return cut_branches; }
void Cutting::setCutBranches(int cut_branches) {
    if(cut_branches < 0) {
        throw std::invalid_argument( "Сut_branches: negative value received" );
    }
    else {
        this->cut_branches = cut_branches;
    }
}

std::string Cutting::toString() const {
    std::string str;
    struct tm *timeinfo;
    timeinfo = localtime ( &date );
    str = "Date: " + std::string(std::asctime(timeinfo));
    str += "Student: " + std::string(student) + "\n";
    str += "Course: " + std::to_string(course) + "\n";
    str += "Cut branches: " + std::to_string(cut_branches) + "\n";
    return str;
}

std::size_t Cutting::toBytes(std::byte *&ptr) const {
    std::size_t len = sizeof(int) + sizeof(Cutting) -
                      - sizeof(char*) + (strlen(student) + 1);
    ptr = new std::byte[len];
    auto tmp = ptr;
    *(int*)tmp = CUTTING_RECORD;
    tmp += sizeof(int);
    memcpy(tmp, &date, sizeof(date));
    tmp += sizeof(date);
    memcpy(tmp, &course, sizeof(course));
    tmp += sizeof(course);
    memcpy(tmp, &cut_branches, sizeof(cut_branches));
    tmp += sizeof(cut_branches);
    //ФИО записывается в конец, чтобы не хранить его длину
    memcpy(tmp, student, (strlen(student) + 1));
    return len;
}

void Cutting::fromBytes(std::byte *ptr, std::size_t len) {
    std::size_t name_len = len - sizeof(date)
                           - sizeof(course) - sizeof(cut_branches);
    memcpy(&date, ptr, sizeof(date));
    ptr += sizeof(date);
    memcpy(&course, ptr, sizeof(course));
    ptr += sizeof(course);
    memcpy(&cut_branches, ptr, sizeof(cut_branches));
    ptr += sizeof(cut_branches);
    student = new char[name_len];
    memcpy((char *) student, ptr, name_len);
}

bool Cutting::operator==(const Cutting &object) const {
    if(this->CareRecord::operator!=(object)) return false;
    if(this->cut_branches != object.cut_branches) return false;
    return true;
}

bool Cutting::operator!=(const Cutting &object) const {
    return !(*this == object);
}

RecordTypes Cutting::getType() const {
    return CUTTING_RECORD;
}
