#ifndef CUTTING_H
#define CUTTING_H

#include "Care/carerecord.h"

class Cutting : public CareRecord {
private:
    int cut_branches;

public:
    Cutting();
    Cutting(time_t date, const char* student, int course, int cut_branches);
    Cutting(const Cutting &object);
    ~Cutting() override;
    [[nodiscard]] int getCutBranches() const;
    void setCutBranches(int cut_branches);
    [[nodiscard]] RecordTypes getType() const override;
    [[nodiscard]] std::string toString() const override;
    [[nodiscard]] std::size_t toBytes(std::byte *&ptr) const override;
    void fromBytes(std::byte *ptr, std::size_t len);
    bool operator==(const Cutting &object) const;
    bool operator!=(const Cutting &object) const;
};

#endif // CUTTING_H
