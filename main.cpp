#include "Interface/mainwindow.h"

#include "Vector/vector_test.cpp"
#include "Care/care_test.cpp"
#include "Cutting/cutting_test.cpp"
#include "Watering/watering_test.cpp"
#include "Collection/collection_test.cpp"

#include <QApplication>
#include <QLocale>
#include <QTranslator>

void printCuttingLog(const CuttingLog &log);
void printWateringLog(const WateringLog &log);

int main(int argc, char *argv[])
{

///#############################################################///
///                                                             ///
///          Классы Watering и Cutting очень похожи             ///
///         (как и классы WateringLog и CuttingLog),            ///
///       что будет исправлено согласно заданию 3 части.        ///
///                                                             ///
///#############################################################///

    printf("------------Начало тестирования-----------\n");

    //##########################################################

    printf("Тестирование CareRecord...\n");
    testCareRecord();
    printf("Тестирование CareRecord пройдено.\n");

    //##########################################################

    printf("Тестирование Cutting...\n");
    testCuttingRecord();
    printf("Тестирование Cutting пройдено.\n");

    //##########################################################

    printf("Тестирование Watering...\n");
    testWateringRecord();
    printf("Тестирование Watering пройдено.\n");

    //##########################################################

    printf("Тестирование Vector...\n");
    testVector();
    printf("Тестирование Vector пройдено.\n");

    //##########################################################

    printf("Тестирование Collection...\n");
    testCollection();
    printf("Тестирование Collection пройдено.\n");

    //##########################################################

    printf("-----Все стадии тестирования пройдены-----\n");


    QApplication a(argc, argv);
    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "laboratory_work_1_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            a.installTranslator(&translator);
            break;
        }
    }
    MainWindow w;
    w.show();
    return a.exec();

    return 0;
}